import express from 'express'
import fs from 'fs'
import {Latch} from "./latch.mjs";


const app = express()
const port = 3000

app.use(express.static('static'))

app.get('/', (req,res) => {
    res.send('hello world')
})

let shutdownLatch = new Latch()

app.get('/wait-for-shutdown-signal', async (req, res) => {
    await shutdownLatch.waitForSignal()
    res.send(JSON.stringify({'shutdown': true}));
})

app.post('/shutdown', (req, res) => {
    shutdownLatch.sendSignal()
    res.send(JSON.stringify({'success': true}))
})

app.get('/ping', (req, res) => {
    res.send(JSON.stringify({'ok': true}))
})


app.listen(3000, () => {
    console.log(`example app listening on ${port}`)
})