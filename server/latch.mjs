

export class Latch {
    constructor() {
        this.promise = new Promise(resolve => {
            this.resolvePromise = resolve
        })
    }

    async waitForSignal() {
        await this.promise
    }

     sendSignal() {
        this.resolvePromise(null)
    }
}