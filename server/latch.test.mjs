import {Latch} from './latch'
import {jest} from '@jest/globals';

describe("Latch", () => {
    it ("waitForSignal waits until 'sendSignal' is called", async () => {
        const callback = jest.fn();
        const latch = new Latch();
        latch.waitForSignal().then(callback)
        await delay(10)
        expect(callback).not.toHaveBeenCalled()
        latch.sendSignal()
        await delay(10)
        expect(callback).toHaveBeenCalled()
    })

    it("signals multiple callbacks", async () => {
        const callback1 = jest.fn();
        const callback2 = jest.fn();
        const latch = new Latch();
        latch.waitForSignal().then(callback1)
        latch.waitForSignal().then(callback2)
        await delay(10)
        expect(callback1).not.toHaveBeenCalled()
        expect(callback2).not.toHaveBeenCalled()

        latch.sendSignal()
        await delay(10)
        expect(callback1).toHaveBeenCalled()
        expect(callback2).toHaveBeenCalled()
    })

    it("if signal is sent before waiting, don't wait", async () => {
        const callback = jest.fn();
        const latch = new Latch();
        latch.sendSignal()
        latch.waitForSignal().then(callback)
        await delay(10)
        expect(callback).toHaveBeenCalled()
    })
})



function delay(millis) {
    return new Promise(resolve => setTimeout(resolve, millis))
}