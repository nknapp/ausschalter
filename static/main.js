const offButton = document.querySelector("button.off");

const confirmDialog = document.querySelector('.confirmDialog');
const confirmButton = document.querySelector(".confirmButton")
const cancelButton = document.querySelector(".cancelButton")


offButton.addEventListener("click", async () => {
    const confirmation = await waitForConfirm()

    if (confirmation) {
        await fetch("/shutdown", {method: 'POST'})
        checkServerStatusUntilOffline()
    }
})


async function waitForConfirm() {
    confirmDialog.classList.add('show')
    const confirmation = await Promise.race([
        new Promise(resolve => confirmButton.addEventListener('click', () => resolve(true))),
        new Promise(resolve => cancelButton.addEventListener('click', () => resolve(false)))
    ])
    confirmDialog.classList.remove('show')
    return confirmation;
}

function checkServerStatusUntilOffline() {
    const online = checkServerStatus()
    if (online) {
        setTimeout(checkServerStatusJob, 1000)
    }
}

checkServerStatusJob()

function checkServerStatusJob() {
    const online = checkServerStatus()
    if (online) {
        setTimeout(checkServerStatusJob, 10000)
    } else {
        // Try more often if server if offline, in order to
        // notify early
        setTimeout(checkServerStatusJob, 2000   )
    }
}

async function checkServerStatus() {
    const pingReceived = await pingServer({timeout: 500});
    if (pingReceived) {
        offButton.classList.add('status-running')
    } else {
        offButton.classList.remove('status-running')
    }
    return pingReceived
}

async function pingServer({timeout}) {
    const abortController = new AbortController()
    const pingReceived = await Promise.race([
        fetch('/ping', {signal: abortController.signal}).then(() => true),
        new Promise(resolve => setTimeout(resolve, timeout)).then(() => false)
    ])
    if (!pingReceived) {
        abortController.abort()
    }
    return pingReceived;
}

