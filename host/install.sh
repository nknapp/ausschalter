set -e

cd "$( dirname "$( readlink -f "$0" )" )"

sudo cp poweroff-worker.sh /usr/local/bin/
sudo cp poweroff-worker.service /etc/systemd/system/poweroff-worker.service
chown root.root /usr/local/bin/poweroff-worker.sh
chown root.root /etc/systemd/system/poweroff-worker.service

sudo systemctl daemon-reload
sudo systemctl enable poweroff-worker.service
sudo systemctl start poweroff-worker.service