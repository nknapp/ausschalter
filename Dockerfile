FROM node:16-bullseye-slim

RUN mkdir /app
WORKDIR /app
COPY package.json /app/
COPY yarn.lock /app/
RUN yarn install --production
COPY server/ /app/server/
COPY static/ /app/static/
USER root